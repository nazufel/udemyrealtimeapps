// app.js



// parent component to boostrap the rest of the components
// controller view 
class ChannelSection extends React.Component {
    // constrcutor to stub in channels
    constructor(props) {
        super(props);
        this.state = {
            channels: [
                {name: 'Hardware Support'},
                {name: 'Software Support'}
            ]
        }
    }
    // add channel function for adding channels
    // takes the name property from the constructor
    addChannel(name) {
        this.setState((prevState) => {
            return {
                channels: prevState.channels.concat({name: name })
            }
        })
    }
    render() {
        return(
            // wrapping children in div because render() can only return one item
            // returning all child components
            // ChannelForm has access to the addChannel function
            <div>
                <ChannelList channels={this.state.channels} />
                <ChannelForm addChannel={this.addChannel.bind(this)}/>
            </div>
        )
    }
}

// create a component named "Channel"
class Channel extends React.Component {
    onClick() {
        console.log("I was clicked", this.props.name);
    }
    // call the render method
    render() {
        // return the below html to be renderd
        return (
            // set up a list of channels using the propery 'name'
            // using .bind(this) to bind the context to onClick() function rather than the render() function. Scope matters.
            <li onClick={this.onClick.bind(this)}>{this.props.name}</li>
        )
    }
}

// component to create a list of Channels
class ChannelList extends React.Component{
    render() {
        return (
            // return unordered list of channels from the channel map
            <ul>
                {this.props.channels.map( channel => {
                    return (
                        <Channel name={channel.name}/>
                    )
                }
            )}
            </ul>
        )
    }
}

// component to create a channel form for creating channels
class ChannelForm extends React.Component {
    // not sure what constructors do.
    // i guess injecting properies into the component
    constructor(props) {
        super(props);
        this.state = {};
    }
    // define the on change event to do stuff with form value
    onChange(e){
        // set the state channel name of the event target value
        this.setState({
            channelName: e.target.value
        })
    }
    // define the on submit event to do stuff on submittion of form value
    onSubmit(e) {
        // set the channelName array state
        let {channelName} = this.state;
        // log the channel name in the form field
        console.log(channelName);
        // clear out the form for the next submission
        this.setState({
            channelName: ''
        });
        // use the addChannel function to add a channel to the channel array
        this.props.addChannel(channel);
        // prevent event default behavior. TODO: more research on that. 
        e.preventDefault();
    }
    // render the channelForm
    render() {
        return (
            // render the form
            <form onSubmit={this.onSubmit.bind(this)}>
                <input type='text' onChange={this.onChange.bind(this)} value={this.state.channelName}/>
            </form>
        )
    }
}

// JSX - export the component and render to the DOM
ReactDOM.render(<ChannelSection/>, document.getElementById('app'));